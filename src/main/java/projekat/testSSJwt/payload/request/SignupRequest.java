package projekat.testSSJwt.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignupRequest {

    @NotBlank
    String username;

    @NotBlank
    String email;

    private Set<String> role;

    @NotBlank
    private String password;



}
